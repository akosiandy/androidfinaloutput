package com.secretkingsph.apps.finaloutput;

import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;



public class AddUser extends AppCompatActivity {
    DatabaseHelper mydb;
    EditText user,pass,name;
    Button btnadduser;
    Cursor X;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_user);
        mydb = new DatabaseHelper(this);
        user= (EditText) findViewById(R.id.txtuser);
        pass= (EditText) findViewById(R.id.txtpass);
        name= (EditText) findViewById(R.id.txtname);
        btnadduser = (Button) findViewById(R.id.btnadduser);

    }

    public void btnonclick(View view)
    {
        if ((TextUtils.isEmpty(user.getText().toString())) || (TextUtils.isEmpty(pass.getText().toString())) || (TextUtils.isEmpty(name.getText().toString()))){
            Toast.makeText(this, "All 3 Fields Are Required",Toast.LENGTH_SHORT).show();
        }
        else{

            mydb.insertDATASQL(user.getText().toString(),pass.getText().toString(),name.getText().toString());
            Toast.makeText(this,"User Sucessfully added",Toast.LENGTH_SHORT).show();
        }
        user.setText(null);
        pass.setText(null);
        name.setText(null);
//       refreshCURSOR();
    }





    }





