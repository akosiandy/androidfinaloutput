package com.secretkingsph.apps.finaloutput;

/**
 * Created by ICARUSLKCS on 12/18/2017.
 */

        import android.content.Context;
        import android.database.Cursor;
        import android.database.sqlite.SQLiteDatabase;
        import android.database.sqlite.SQLiteOpenHelper;
        import android.util.Log;

public class DatabaseHelper  extends SQLiteOpenHelper{

    public static final String DATABASE_NAME = "login.db";
    public static final String TABLE_NAME = "users";
    public static final String COL_1 = "ID";
    public static final String COL_2 = "Username";
    public static final String COL_3 = "Password";
    public static final String COL_4 = "Name";

    public DatabaseHelper(Context context){
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sqlString;
        sqlString = "create table " + TABLE_NAME + "(ID INTEGER PRIMARY KEY AUTOINCREMENT, Username TEXT, Password TEXT, Name TEXT)";
        db.execSQL(sqlString);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sqlString;
        sqlString = "drop table if exists " + TABLE_NAME;
        db.execSQL(sqlString);
        onCreate(db);
    }

    public void insertDATASQL(String username, String password,String name){
        SQLiteDatabase db = this.getWritableDatabase();
        String sqlString;
        sqlString = "insert into " + TABLE_NAME + " (" + COL_2 + "," + COL_3 + "," + COL_4 + ") VALUES ('" + username + "','" + password + "','" + name + "')";
        db.execSQL(sqlString);
        db.close();
    }

    public Cursor dbCURSOR(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from " + TABLE_NAME, null);
        Log.i("TAG", res.toString());
        return res;
    }
    public boolean checkuser(String username, String password) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("Select * from " + TABLE_NAME + " where Username=? AND Password=?", new String[] {username,password});
        if(cursor.getCount() <= 0){
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

    public void deleteDATA(String dataID){
        SQLiteDatabase db = this.getWritableDatabase();
        String sqlString;
        sqlString = "delete from "+ TABLE_NAME + " WHERE " + COL_1 + " = '" + Integer.parseInt(dataID) + "'";
        db.execSQL(sqlString);
    }
    public void updateuser(String ID,String username,String password,String Name)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        String sqlString;
        sqlString = "UPDATE " + TABLE_NAME + " SET Username= '"+ username +"' , Password='"+ password +"' , Name='"+Name+"'" + " where ID = '" + Integer.parseInt(ID)+"'";
        Log.i("info", sqlString);
        db.execSQL(sqlString);
    }


}

