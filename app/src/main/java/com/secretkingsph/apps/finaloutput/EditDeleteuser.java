package com.secretkingsph.apps.finaloutput;

import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class EditDeleteuser extends AppCompatActivity {
    DatabaseHelper mydb;
    Cursor X;
    EditText txtusername,txtpass,txtname,txtid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_deleteuser);
        mydb = new DatabaseHelper(this);
        X=mydb.dbCURSOR();
        txtusername = (EditText) findViewById(R.id.txtusername);
        txtpass = (EditText) findViewById(R.id.txtpassword);
        txtname = (EditText) findViewById(R.id.txtname);
        txtid = (EditText) findViewById(R.id.txtid);
    }

    public void clickprev(View view)
    {
        if(X.moveToPrevious()){
            txtid.setText(X.getString(0));
            txtusername.setText(X.getString(1));
            txtpass.setText(X.getString(2));
            txtname.setText(X.getString(3));
        }
        else{
            Toast.makeText(this, "Already in First Item", Toast.LENGTH_SHORT).show();
            X.moveToNext();
        }

    }

    public void clicknext(View view)
    {
        if(X.moveToNext()){
            txtid.setText(X.getString(0));
            txtusername.setText(X.getString(1));
            txtpass.setText(X.getString(2));
            txtname.setText(X.getString(3));
        }
        else{
            Toast.makeText(this, "Already in Last", Toast.LENGTH_SHORT).show();
            X.moveToLast();
        }


    }
 public  void clickdelete(View view)
 {
     if ((TextUtils.isEmpty(txtusername.getText().toString())) || (TextUtils.isEmpty(txtpass.getText().toString())) || (TextUtils.isEmpty(txtname.getText().toString()))){
         Toast.makeText(this, "All 3 Fields Are Required",Toast.LENGTH_SHORT).show();
     }
     else{

         mydb.deleteDATA(txtid.getText().toString());

         txtid.setText(null);
         txtusername.setText(null);
         txtpass.setText(null);
         txtname.setText(null);
         refreshCURSOR();
         Toast.makeText(this, "Deleted",Toast.LENGTH_SHORT).show();
     }



 }
    public void refreshCURSOR(){
        int refreshPos = X.getPosition();
        X = mydb.dbCURSOR();
        X.moveToPosition(refreshPos);
    }


    public void clickclear(View view)
    {
        txtid.setText(null);
        txtusername.setText(null);
        txtpass.setText(null);
        txtname.setText(null);
    }
    
    public void adduser(View view)
    {
        if ((TextUtils.isEmpty(txtusername.getText().toString())) || (TextUtils.isEmpty(txtpass.getText().toString())) || (TextUtils.isEmpty(txtname.getText().toString()))){
            Toast.makeText(this, "All 3 Fields Are Required",Toast.LENGTH_SHORT).show();
        }
        else{

            mydb.insertDATASQL(txtusername.getText().toString(),txtpass.getText().toString(),txtname.getText().toString());
            Toast.makeText(this,"txtusername Sucessfully added",Toast.LENGTH_SHORT).show();
        }
        txtusername.setText(null);
        txtpass.setText(null);
        txtname.setText(null);
        refreshCURSOR();
    }

    public void edituser(View view)
    {
        if ((TextUtils.isEmpty(txtusername.getText().toString())) || (TextUtils.isEmpty(txtpass.getText().toString())) || (TextUtils.isEmpty(txtname.getText().toString()))){
            Toast.makeText(this, "Error Editing ",Toast.LENGTH_SHORT).show();
        }
        else{

            mydb.updateuser(txtid.getText().toString(),txtusername.getText().toString(),txtpass.getText().toString(),txtname.getText().toString());
            Toast.makeText(this,"Successful Edit",Toast.LENGTH_SHORT).show();
        }

    }


}
