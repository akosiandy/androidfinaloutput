package com.secretkingsph.apps.finaloutput;


import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    DatabaseHelper mydb;
    EditText username ;
    EditText pass;
    Cursor X;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mydb = new DatabaseHelper(this);
        username = (EditText) findViewById(R.id.txtusername);
        pass = (EditText) findViewById(R.id.txtpass);
    }


    public void oclicklogin(View view)
    {

        if (username.getText().toString().equals("adduser"))
        {
            Intent intent = new Intent(getApplicationContext(),AddUser.class);
            startActivity(intent);
        }
        else if (mydb.checkuser(username.getText().toString(),pass.getText().toString()))
        {
            Toast.makeText(this, "Login Success",Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(getApplicationContext(),EditDeleteuser.class);
            startActivity(intent);
        }
        else
        {
            Toast.makeText(this, "Login FAILED",Toast.LENGTH_SHORT).show();
        }

    }


}
